package com.askerlap.emad.nutritionanalysistask.repositories

import com.askerlap.emad.nutritionanalysistask.network.NutritionApiHelper
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val apiHelper: NutritionApiHelper
){

    suspend fun getIngredientAnalysis(param: String) = apiHelper.getNutritionAnalysis(param)
}