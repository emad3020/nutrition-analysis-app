package com.askerlap.emad.nutritionanalysistask.network

import com.askerlap.emad.nutritionanalysistask.models.ApiResponse
import com.askerlap.emad.nutritionanalysistask.models.RequestModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface NutritionApi {

    @GET("api/nutrition-data")
    suspend fun getNutritionAnalysis(@Query("ingr") ingredient: String): Response<ApiResponse>
}