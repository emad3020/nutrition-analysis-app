package com.askerlap.emad.nutritionanalysistask.ui.summaryScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.askerlap.emad.nutritionanalysistask.R
import com.askerlap.emad.nutritionanalysistask.databinding.IngredientRowItemBinding

class IngredientAdapter(private val ingredientList: List<String>,private val itemClick: (String) -> Unit) :
    RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: IngredientRowItemBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.ingredient_row_item,
                parent,
                false
            )
        return IngredientViewHolder(binding,itemClick)
    }


    override fun onBindViewHolder(holder: IngredientViewHolder, position: Int) {

        holder.bindItems(ingredientList[position])
    }

    override fun getItemCount(): Int = ingredientList.count()


    inner class IngredientViewHolder(
        private val mBinding: IngredientRowItemBinding,
        clickFunc: (String) -> Unit
    ) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener{
                clickFunc(ingredientList[layoutPosition])
            }
        }

        fun bindItems(item: String) {
            mBinding.apply {
                tvFoodIngredient.text = item
            }
        }
    }
}