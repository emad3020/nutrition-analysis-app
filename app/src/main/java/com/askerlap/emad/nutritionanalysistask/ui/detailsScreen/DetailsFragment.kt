package com.askerlap.emad.nutritionanalysistask.ui.detailsScreen

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import com.askerlap.emad.nutritionanalysistask.bases.BaseFragment
import com.askerlap.emad.nutritionanalysistask.databinding.FragmentDetailsBinding
import com.askerlap.emad.nutritionanalysistask.models.ApiResponse
import com.askerlap.emad.nutritionanalysistask.ui.summaryScreen.SummaryViewModel
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import java.text.DecimalFormat

@AndroidEntryPoint
class DetailsFragment : BaseFragment<FragmentDetailsBinding>(FragmentDetailsBinding::inflate) {

     val mViewModel: SummaryViewModel by viewModels()
     private val df = DecimalFormat("#.##")


    override fun FragmentDetailsBinding.initialize() {
        this.viewModel = mViewModel
        this.lifecycleOwner = lifecycleOwner
        val data = arguments?.getSerializable("ingredient_result") as ApiResponse
        tvCalories.text = "${data.calories} Calories"
        tvWeight.text = "${data.totalWeight} Weight"
        tvIngredientTitle.text = arguments?.getString("ingredient_title","")

        data.totalNutrients.forEach {
            val chip = Chip(requireContext())
            chip.text = "${df.format(it.value.quantity)} ${it.value.unit} ${it.value.label}"
            cgQuantity.addView(chip)
        }

    }

    override fun registerObserver() {

    }

}