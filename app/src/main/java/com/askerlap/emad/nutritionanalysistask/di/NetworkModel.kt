package com.askerlap.emad.nutritionanalysistask.di

import com.askerlap.emad.nutritionanalysistask.BuildConfig
import com.askerlap.emad.nutritionanalysistask.network.NutritionApi
import com.askerlap.emad.nutritionanalysistask.network.NutritionApiHelper
import com.askerlap.emad.nutritionanalysistask.network.NutritionApiHelperImpl
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModel {

    @Provides
    fun provideBaseUrl(): String = BuildConfig.BASE_URL

    @Singleton
    @Provides
    fun getGson(): Gson = GsonBuilder()
        .setLenient()
        .create()

    inline fun <reified T : Any> getService(retrofit: Retrofit): T? {
        return retrofit.create(T::class.java)
    }


    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor() : HttpLoggingInterceptor {
        val debugInterceptor = HttpLoggingInterceptor()
        debugInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return debugInterceptor
    }

    @Singleton
    @Provides
    fun provideOkHttpClientBuilder(debugInterceptor: HttpLoggingInterceptor) : OkHttpClient.Builder {
        val builder = OkHttpClient.Builder()
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.connectTimeout(60, TimeUnit.SECONDS)
        builder.addInterceptor(debugInterceptor)
        return builder
    }

    @Singleton
    @Provides
    fun provideOkhttpClient(builder: OkHttpClient.Builder): OkHttpClient {

        builder.addInterceptor { chain ->
            var request = chain.request()
            val urlRequest = request.url.newBuilder()
            urlRequest.addQueryParameter("app_id", BuildConfig.APIID)
            urlRequest.addQueryParameter("app_key",BuildConfig.APIKEY)
            request = request.newBuilder().url(urlRequest.build())
                .addHeader("Api-Access", "application/mobile")
                .addHeader("content-type", "application/json; charset=utf-8")
                .build()
            chain.proceed(request)
        }
        return builder.build()
    }

    @Singleton
    @Provides
    fun provideGsonConverter(gsonClient: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gsonClient)

    @Singleton
    @Provides
    fun getRetrofitClient(okHttpClient: OkHttpClient, baseUrl: String,converter: GsonConverterFactory): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(converter)
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit) = retrofit.create(NutritionApi::class.java)

    @Singleton
    @Provides
    fun provideApiHelper(apiHelper: NutritionApiHelperImpl): NutritionApiHelper = apiHelper
}