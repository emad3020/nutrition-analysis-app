package com.askerlap.emad.nutritionanalysistask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.askerlap.emad.nutritionanalysistask.databinding.ActivityMainBinding
import com.askerlap.emad.nutritionanalysistask.models.RequestModel
import com.askerlap.emad.nutritionanalysistask.di.NetworkModel
import com.askerlap.emad.nutritionanalysistask.models.ApiResponse
import com.askerlap.emad.nutritionanalysistask.network.NutritionApi
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private var mBinding: ActivityMainBinding? = null
    @Inject lateinit var retrofit: Retrofit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        mBinding?.lifecycleOwner = this
    }

}