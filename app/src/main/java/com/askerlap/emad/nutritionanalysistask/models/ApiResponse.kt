package com.askerlap.emad.nutritionanalysistask.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ApiResponse(
    val uri: String? = "",
    val yield: Float? = 0f,
    val calories: Long? = 0L,
    val totalWeight: Double = 0.0,
    val totalNutrients: Map<String, TotalDaily>,
    val totalDaily: Map<String, TotalDaily>,
    val totalNutrientsKCal: TotalNutrientsKCal
): Serializable

data class TotalDaily(
    val label: String,
    val quantity: Double,
    val unit: String
): Serializable

data class TotalNutrientsKCal(
    @SerializedName("ENERC_KCAL")
    val enercKcal: TotalDaily,

    @SerializedName("PROCNT_KCAL")
    val procntKcal: TotalDaily,

    @SerializedName("FAT_KCAL")
    val fatKcal: TotalDaily,

    @SerializedName("CHOCDF_KCAL")
    val chocdfKcal: TotalDaily
): Serializable