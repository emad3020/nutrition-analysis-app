package com.askerlap.emad.nutritionanalysistask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Singleton

@HiltAndroidApp
class AppDelegate : Application() {
}