package com.askerlap.emad.nutritionanalysistask.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}