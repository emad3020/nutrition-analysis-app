package com.askerlap.emad.nutritionanalysistask.network

import com.askerlap.emad.nutritionanalysistask.models.ApiResponse
import dagger.Provides
import retrofit2.Response

interface NutritionApiHelper {
    suspend fun getNutritionAnalysis(ingredient: String): Response<ApiResponse>
}