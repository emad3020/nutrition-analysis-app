package com.askerlap.emad.nutritionanalysistask.ui.summaryScreen

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.askerlap.emad.nutritionanalysistask.models.ApiResponse
import com.askerlap.emad.nutritionanalysistask.repositories.MainRepository
import com.askerlap.emad.nutritionanalysistask.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SummaryViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {

    private val _res = MutableLiveData<Resource<ApiResponse>>()

    val res: LiveData<Resource<ApiResponse>>
        get() = _res
    var selectedItem  = ObservableField<String>()

    fun loadData(s : String) {
        selectedItem.set(s)
        viewModelScope.launch {
            _res.postValue(Resource.loading(null))
            mainRepository.getIngredientAnalysis(s).let {
                if (it.isSuccessful) {
                    _res.postValue(Resource.success(it.body()))
                } else {
                    _res.postValue(Resource.failure(it.errorBody().toString(),null))
                }
            }
        }
    }
}