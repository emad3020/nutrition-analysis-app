package com.askerlap.emad.nutritionanalysistask.models

import java.io.Serializable


data class RequestModel(
    val title: String? = "",
    val prep: String? = "",
    val yield: String? = "",
    var ingr: List<String>? = listOf()
): Serializable
