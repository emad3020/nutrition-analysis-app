package com.askerlap.emad.nutritionanalysistask.ui

import androidx.navigation.fragment.findNavController
import com.askerlap.emad.nutritionanalysistask.bases.BaseFragment
import com.askerlap.emad.nutritionanalysistask.databinding.FragmentHomeBinding
import com.askerlap.emad.nutritionanalysistask.ui.summaryScreen.SummaryViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    override fun FragmentHomeBinding.initialize() {
        this.lifecycleOwner = lifecycleOwner
        button.setOnClickListener {
            val ingredient = tvResult.text.toString()
            val action = HomeFragmentDirections.actionHomeFragmentToSummaryFragment(ingredient)
            navigate(action)
        }

    }

    override fun registerObserver() {

    }
}