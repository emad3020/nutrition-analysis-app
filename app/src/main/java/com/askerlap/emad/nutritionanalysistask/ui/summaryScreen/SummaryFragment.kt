package com.askerlap.emad.nutritionanalysistask.ui.summaryScreen

import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.askerlap.emad.nutritionanalysistask.bases.BaseFragment
import com.askerlap.emad.nutritionanalysistask.databinding.FragmentSummaryBinding
import com.askerlap.emad.nutritionanalysistask.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SummaryFragment : BaseFragment<FragmentSummaryBinding>(FragmentSummaryBinding::inflate) {

    private val mViewModel : SummaryViewModel by viewModels()

    override fun FragmentSummaryBinding.initialize() {
        viewModel = mViewModel
        this.lifecycleOwner = lifecycleOwner
        val ingredient = arguments?.getString("result","")
        val adapter = IngredientAdapter(ingredient?.split("\n") ?: listOf()){
            mViewModel.loadData(it)
        }
        rvIngredient.run {
            layoutManager = LinearLayoutManager(requireContext())
        }

        this.adapter = adapter
    }

    override fun registerObserver() {
        mViewModel.res.observe(this@SummaryFragment){
            when(it.status) {
                Status.LOADING -> {
                    mBinding?.progressBar?.isVisible = true
                }

                Status.SUCCESS -> {
                    mBinding?.progressBar?.isVisible = false
                    it.data?.let { response ->
                        val action = SummaryFragmentDirections.actionSummaryToDetails(response,mViewModel.selectedItem.get() ?: "")
                        navigate(action)
                    }

                }
                else -> {
                    showToast(it.message ?: "Can't load data, try again!")
                }
            }

        }
    }

}