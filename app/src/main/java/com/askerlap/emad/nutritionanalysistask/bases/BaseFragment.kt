package com.askerlap.emad.nutritionanalysistask.bases

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<T : ViewBinding>(private val inflateFunc: (LayoutInflater, ViewGroup?, Boolean) -> T) :
    Fragment() {

    private var _binding: T? = null

    open val mBinding: T?
        get() = _binding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflateFunc.invoke(inflater, container, false)
        _binding?.initialize()
        registerObserver()
        return _binding?.root
    }

    open fun T.initialize() {}

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    protected abstract fun registerObserver()

    fun showToast(msg: String){
        Toast.makeText(requireContext(),msg,Toast.LENGTH_LONG).show()
    }

    fun navigate(action: NavDirections){
        _binding?.root?.findNavController()?.navigate(action)
    }


}
