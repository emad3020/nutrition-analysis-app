package com.askerlap.emad.nutritionanalysistask.network

import com.askerlap.emad.nutritionanalysistask.models.ApiResponse
import retrofit2.Response
import javax.inject.Inject


class NutritionApiHelperImpl @Inject constructor(
    private val nutritionApi: NutritionApi
) : NutritionApiHelper {
    override suspend fun getNutritionAnalysis(ingredient: String): Response<ApiResponse> =
        nutritionApi.getNutritionAnalysis(ingredient)
}